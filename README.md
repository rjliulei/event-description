# 应用软件系统开发实训室说明

[枣科内部项目入口](https://gitee.com/zaozhuang-vocational-college-it/event-description.git)如提示没有权限，联系我或者学长加入项目。

## 一、收获与付出：

1. 收获：个人能力的提升、知识的学习、升本、评优、奖金？
2. 付出：没有休息日，没有娱乐，没有对象？

## 二、实训室要求：

1.  听指挥
2.  坐得住（要有耐心，还有能扛得住诱惑）
3. 学得快（自学能力要加强），一个问题你研究一个小时解决不了，可以询问学长、同学、小组讨论。两个小时解决不了再问我。
4.  心态好，不要自卑也不能自负。
5.  团队协作精神。不计较一时得失，共同进步。能力越大责任也就越大。
6.  禁止打游戏（至少实训室内）
7.  末位淘汰制，你只要踏实学就不会被淘汰，当然你坚持不下来，不用淘汰你自己就不来了。总之一句话，有成效的坚持你一定会成功！

## 三、赛项：

本实训室重点为<font color='red' style='bold' size=4>编程算法应用类</font>赛项。

1. <font color='red' style='bold' size=4>应用软件系统开发</font>（省赛、一类赛、东软/中慧）：
   1. 软件设计：原型设计（Axture）、ER图（Visio）、流程图、用例图等的绘制
   2. 软件开发（重点）：前端（Vue、HTML、CSS、JavaScript、ECharts）、后端（Java、SpringBoot、MySQL、Redis）、打包部署（Nginx、npm、Maven）
   3.  软件测试：功能测试、缺陷（Bug）修复、接口测试（JMeter）

2.  <font color='#cc0000' style='bold' size=4>程序设计员Python</font>（行赛、二类赛、中教畅享）：
    1. Python语言基础与算法设计
    2. Python网页与文本分析（BeautifulSoup、Request、SnowLP、Jieba）
    3. Python数据处理与分析（Pandas）

3. <font color='#aa0000' style='bold' size=4>网络搭建</font>（省赛、信息通信网络运行管理员）：不是本实训室的重点赛项，属于网络实训室，感兴趣的可私下沟通。

## 四、计划：先期规划，随各赛项随时变动。

1. 各项技术一个月学习一项。Java（MySQL）、SpringBoot、Vue（HMTL、CSS、JS）、Python

2. 每两周或一个月组织一次考试，并计入排名。

3. 具体的每月计划（待完善）。

4. 每一到两周组织一次共同学习，由学生主讲。整理学生问题，统一回复讲解。

## 五、寒假计划：

1. Java基础及高级编程（多线程、异常、泛型、算法）。
2. Python编程基础。
3. HTML（CSS、JS）网页编程基础。
4. 寒假后一至两周测试。

## 六、学习资源推荐：

1. 赛项相关网站：
   1. <a href="https://sdskills.sdei.edu.cn/Default.aspx">山东省职业院校技能大赛（一类赛）</a>
   2. <a href="http://python.itmc.cn/">全国行业职业技能竞赛计算机程序设计员（Python方向）赛项（二类赛）</a>
2. 学习网站：
   1. <a href="https://www.nowcoder.com/">牛客网</a>
   2. <a href="https://leetcode.cn/">力扣网（Python赛项推荐学习）</a>
3. 学习教程推荐：
   1. Java（待完善）
   2. HTML（待完善）
   3. Vue（待完善）
        <a href="https://cn.vuejs.org/">Vue官方文档</a>
        <a href="https://element.eleme.cn/#/zh-CN">饿了么组件（常用）</a>
   4. SpringBoot（待完善）
   5. MySQL（待完善）
   6. Python（待完善）
   7. Pandas（待完善）

 ## 七、各目录说明

1. 样题：仅放一到两份样题，完整内容放到资源库中。Python赛项每个模块放1至2道题。（待完善）

## 八、各仓库说明

1. <a href="https://gitee.com/rjliulei/event-description">赛项说明库</a>：赛项说明、技术说明、学习资料推荐、软件、部分样题等

2. 练习库（待完善）：新生练习、练习题库（互相出题，每个人都可以提交练习题）

3. 资源库（待完善）：各类赛项资源



本说明最终解释权归指导老师柳磊:happy:。
